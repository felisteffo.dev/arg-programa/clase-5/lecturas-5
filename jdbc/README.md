
# Apunte 7 - JDBC (Java Database Connectivity)

En la clase nos centraremos en cómo Java se conecta e interactúa con bases de datos a través de JDBC. Aprenderemos los fundamentos de JDBC, cómo configurar conexiones a diferentes bases de datos y cómo realizar operaciones básicas.

## JDBC: Conectando Java con Bases de Datos

**JDBC (Java Database Connectivity)** conecta una aplicación Java a una fuente de datos como puede ser una DB.

Es una API que es usada para:

* Conectar a una fuente de datos
* Enviar consultas y actualizaciones
* Recupera y procesa el resultado

JDBC utiliza drivers para conectarse a la DB.

![Apuntes de Clase](../images/JDBC_01.png)

### JDBC - API(Application Programming Interfaces)

Es un conjunto de definiciones y protocolos que se utiliza para desarrollar e integrar el software de las aplicaciones, permitiendo la comunicación a través de un conjunto de reglas.

![Apuntes de Clase](../images/JDBC_02.png)

![Apuntes de Clase](../images/JDBC_03.png)

### JDBC - Implementación

JDBC Driver es un componente de software que habilita a la aplicación java a interactuar con la DB.

Existen 4 Tipos de Drivers Jdbc

* **Tipo 1 - Bridge:** Controladores que implementan la API JDBC como una asignación a otra API de acceso a datos, como ODBC. El JDBC-ODBC Bridge es un ejemplo de un driver de Tipo 1.
    > Nota: El JDBC-ODBC Bridge debe considerarse solo si su DBMS no ofrece un controlador JDBC solo para Java y como una solución de transición.
* **Tipo 2 - Native:** Los controladores que se escriben en parte en el lenguaje de programación Java y en parte en código nativo. Estos controladores utilizan una biblioteca de cliente nativa específica para el origen de datos al que se conectan. El driver Oracle controlador del lado del cliente OCI (Oracle Call Interface) de Oracle es un ejemplo de un driver Tipo 2.
* **Tipo 3 - Network:** Controladores que utilizan un cliente Java puro y se comunican con un servidor middleware mediante un protocolo independiente de la base de datos. El servidor middleware entonces comunica las peticiones del cliente al origen de datos.
* **Tipo 4 – Thin o Pure Java:** Los controladores son Java puro e implementan el protocolo de red para una fuente de datos específica. El cliente se conecta directamente al origen de datos. El driver Oracle Thin es un ejemplo de driver Tipo 4.

## Para interactuar con una DB debemos considerar 5 pasos

1. Cargar el driver necesario para comprender el protocolo que usa la base de datos concreta 
2. Establecer una conexión con la base de datos, normalmente a través de red
3. Enviar consultas SQL y procesar el resultado
4. Liberar los recursos al terminar
5. Manejar los errores que se puedan producir

### 1. Registrar driver

Antes de poder conectarse a la base de datos es necesario cargar el driver JDBC. Sólo hay que hacerlo una única vez al comienzo de la aplicación:

```java
Class.forName("com.mysql.jdbc.Driver");
```

El nombre del driver debe venir especificado en la documentación de la base de datos.

Se puede elevar la excepción ClassNotFoundException si hay un error en el nombre del driver o si el fichero .jar no está correctamente en el CLASSPATH o en el proyecto

![Apuntes de Clase](../images/JDBC_05.png)

Otra alternativa para lograr el mismo objetivo es utilizar el mecanismo que internamente usa la clase Driver, esto es invocar el método registerDriver de la clase DriverManager.

```java
DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
```

En este caso evitamos la excepción ClassNotFoundException porque de no estar la clase no hubiera compilado pero; es necesario contar con el driver (librería.jar) para compilar la clase.

### 2. Crear el objeto de conexión

Las bases de datos actúan como servidores y las aplicaciones como clientes que se comunican a través de la red. Un objeto `Connection` representa una conexión física entre el cliente y el servidor. Para crear una conexión se usa la clase DriverManager donde se especifica la URL, el nombre y la contraseña:

```java
Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bd", "usr", "pass");
```

El formato de la URL debe especificarse en el manual de la base de datos.

Ejemplo de Mysql con el driver JDBC:

```shell
    jdbc:mysql://\<host>:\<puerto>:\<instancia>
```

Por ejemplo:

```shell
jdbc:mysql://localhost:3306/miBD
```

> El nombre de usuario y la contraseña dependen también de la base de datos.

![Apuntes de Clase](../images/JDBC_06.png)

Cada objeto `Connection` representa una conexión física con la base de datos.

Se pueden especificar más propiedades además del usuario y la password al crear una conexión. Estas propiedades se pueden especificar usando métodos `getConnection(…)` sobrecargados de la clase `DriverManager`.

#### Alternativas para crear Conexiones a la base de datos 

```java
String url = "jdbc:mysql://localhost:3306/sample";
String name = "root";
String password = "pass" ;
Connection c = DriverManager.getConnection(url, user, password);
```

```java
String url = 
   "jdbc:mysql://localhost:3306/sample?user=root&password=pass";
Connection c = DriverManager.getConnection(url);
```

```java
String url = "jdbc:mysql://localhost:3306/sample";
Properties prop = new Properties();
prop.setProperty("user", "root");
prop.setProperty("password", "pass");
Connection c = DriverManager.getConnection(url, prop);

```

### 3. Crear la sentencia

Esta sentencia es la responsable de ejecutar las consultas a la DB. Una vez que tienes una conexión puedes ejecutar sentencias SQL:

* Primero se crea el objeto `Statement` desde la conexión
* Posteriormente se ejecuta la consulta y su resultado se devuelve como un `ResultSet`.

```java
Statement stmt = conn.createStatement();
ResultSet rs = stmt.executeQuery("SELECT ... FROM ... ");
```

#### Uso de Statement

Tiene diferentes métodos para ejecutar una sentencia

* `executeQuery(...)`: Se usa para sentencias SELECT. Devuelve un ResultSet.
* `executeUpdate(…)`: Se usa para sentencias INSERT, UPDATE, DELETE o sentencias DDL. Devuelve el número de filas afectadas por la sentencia.
* `execute(…)`: Método genérico de ejecución de consultas. Puede devolver uno o más ResulSet y uno o más contadores de filas afectadas.

![Apuntes de Clase](../images/JDBC_07.png)

#### Acceso al conjunto de resultados

El `ResultSet` es el objeto que representa el resultado. No carga toda la información en memoria, internamente tiene un cursor que apunta a un fila concreta del resultado en la base de datos. 

Hay que posicionar el cursor en cada fila y obtener la información de la misma.

![Apuntes de Clase](../images/JDBC_09.png)

#### Posicionamiento del cursor

* El cursor puede estar en una fila concreta.
* También puede estar en dos filas especiales.
  * Antes de la primera fila (Before the First Row, BFR)
  * Después de la última fila (After the Last Row, ALR)  
* Inicialmente el `ResultSet` está en BFR. 
* `next()` mueve el cursor hacia delante, 
  * devuelve `true` si se encuentra en una fila concreta 
  * y `false` si alcanza el ALR.

![Apuntes de Clase](../images/JDBC_08.png)

#### Obtención de los datos de la fila

Cuando el `ResultSet` se encuentra en una fila concreta se pueden usar los métodos de acceso a las columnas:

* String getString(String columnLabel)
* String getString(int columnIndex)
* int getInt(String columnLabel)
* int getInt(int columnIndex)
* … (existen dos métodos por cada tipo) 

![Apuntes de Clase](../images/JDBC_10.png)

### 4. Liberar recursos

Cuando se termina de usar una `Connection`, un `Statement` o un `ResultSet` es necesario liberar los recursos que necesitan.

Puesto que la información de un ResultSet no se carga en memoria, existen conexiones de red abiertas.

**Métodos close():**

* `ResultSet.close()` – Libera los recursos del ResultSet. Se cierra automáticamente al cerrar el Statement que lo creó o al reejecutar el Statement.
* `Statement.close()` – Libera los recursos del Statement.
* `Connection.close()` – Finaliza la conexión con la base de datos

![Apuntes de Clase](../images/JDBC_11.png)

### 5. Manejar los errores

Hay que gestionar los errores apropiadamente: 

* Se pueden producir excepciones `ClassNotFoundException` si no se encuentra el driver.
* Se pueden producir excepciones `SQLException` al interactuar con la base de datos
  * SQL mal formado
  * Conexión de red rota
  * Problemas de integridad al insertar datos (claves duplicadas)

![Apuntes de Clase](../images/JDBC_12.png)

![Apuntes de Clase](../images/JDBC_13.png)

## Sentencias SQL

Con JDBC se pueden usar diferentes tipos de Statement:

* `Statement:` SQL estático en tiempo de ejecución, no acepta parámetros.

  ```java
    Statement stmt = conn.createStatement();
  ```

* `PreparedStatement`: Para ejecutar la mismas sentencia muchas veces (la “prepara”). Acepta parámetros
  
    ```java
    PreparedStatement ps = conn.prepareStatement(...);
    ```

* `CallableStatement`: Llamadas a procedimientos almacenados

    ```java
    CallableStatement s = conn.prepareCall(...);
    ```

### Ejemplo JDBC

```java
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPASample {
  public static void main(String[] args) {
    // Crear EntityManagerFactory
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence-unit");
    
    // Crear EntityManager
    EntityManager em = emf.createEntityManager();
    
    // Iniciar transacción
    em.getTransaction().begin();
    
    // Operación CRUD
    MyEntity entity = new MyEntity();
    entity.setName("Sample");
    em.persist(entity);
    
    // Commit
    em.getTransaction().commit();
    
    // Cerrar recursos
    em.close();
    emf.close();
  }
}

```

## Enlaces relacionados

* [https://www.xataka.com/basics/api-que-sirve](https://www.xataka.com/basics/api-que-sirve)
* [https://www.javatpoint.com/java-jdbc](https://www.javatpoint.com/java-jdbc)
* [https://dev.mysql.com/downloads/connector/j/](https://dev.mysql.com/downloads/connector/j/)
* [https://codigoxules.org/conectar-mysql-utilizando-driver-jdbc-java-mysql-jdbc/](https://codigoxules.org/conectar-mysql-utilizando-driver-jdbc-java-mysql-jdbc/)
