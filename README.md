# Clase 5

Materiales para la Quinta clase, en esta clase cubrimos:

- Mecanismos de Persistencia de datos en aplicaciones
- Elementos de Bases de Datos Relacionales
- Revisión de JDBC

## Lista de Materiales

- [Apunte 07 - JDBC API](./jdbc/README.md)

## Lista de Ejemplos

- ver ejemplos de JDBC en proyecto Ejemplos Clase

## Estado general de la semana

Versión para publicación 2023.

***

## Software requerido

- Java
- Maven
- IntelliJ idea
- MySql
- SqLite
- DBeaver
- Docker

## Clonar el presente repositorio

``` bash
cd existing_repo
git remote add origin https://gitlab.com/felisteffo.dev/arg-programa/clase-4/lecturas-5.git
git branch -M main
git push -uf origin main
```

## Autores

Felipe Steffolani - basado en un material original creado para la Cátedra de Backend de Aplicaciones

## License

Este trabajo está licenciado bajo una Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional. Para ver una copia de esta licencia, visita [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es](!https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es).
